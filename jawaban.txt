Soal 1 Membuat Database
create Database myshop;
use myshop;



Soal 2 Membuat Table di Dalam Database
a.Tabel users
CREATE TABLE users(
id int auto_increment PRIMARY KEY,
name varchar(255),
email varchar(255),
password varchar(255));

b.Tabel categories
CREATE TABLE categories(
id int auto_increment PRIMARY KEY,
name varchar(255));

c.Tabel Items
CREATE TABLE items(
id int auto_increment PRIMARY KEY,
name varchar(255),
description varchar(255),
price int,
stock int,
category_id int,
FOREIGN KEY(category_id) REFERENCES categories(id)); 



Soal 3 Memasukkan Data pada Table
a.Tabel users
INSERT INTO users (name,email,password) VALUES
("John Doe","john@doe.com","john123"),
("Jane Doe","jane@doe.com","jenita123");

b.Tabel categories
INSERT INTO categories (name) VALUES
("gadget"),
("cloth"),
("men"),
("women"),
("branded");

c.Tabel Items
INSERT INTO items (name,description,price,stock,category_id) VALUES
("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),
("Uniklooh","baju keren dari brand ternama",500000,50,2),
("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);



Soal 4 Mengambil Data dari Database
a. Mengambil data users
SELECT id,name,email,password FROM users;

b. Mengambil data items
SELECT price FROM items WHERE price > 1000000;
SELECT name FROM items WHERE name LIKE '%watch' OR name LIKE 'uniklo%' OR name LIKE '%sang%';

c. Menampilkan data items join dengan kategori
SELECT a.name,a.description,a.price,a.stock,b.id AS kategori FROM
items a,categories b WHERE a.category_id = b.id;



Soal 5 Mengubah Data dari Database
UPDATE items SET price = 2500000 where name ="sumsang b50";